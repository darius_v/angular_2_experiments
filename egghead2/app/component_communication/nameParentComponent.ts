// some comments with this topic added to todoItemRenderer
// https://angular.io/docs/ts/latest/cookbook/component-communication.html

import {Component} from 'angular2/core';
import {NameChildComponent} from './nameChildComponent';
import {VersionParentComponent} from './version/versionParentComponent';

@Component({
	selector: 'name-parent',
	template: `
    <h2>Master controls {{names.length}} names</h2>
    <!-- pass class property to child template using [name] -->
    <name-child *ngFor="#name of names"
      [name]="name">
    </name-child>

    <!-- ngOnChanges example -->
	<version-parent></version-parent>
  `,
	// NameChildComponent has selector name-child
	directives: [NameChildComponent, VersionParentComponent]
})
export class NameParentComponent {
	// Displays 'Mr. IQ', '<no name set>', 'Bombasto'
	names = ['Mr. IQ', '   ', '  Bombasto  '];
}