import {Component} from 'angular2/core'  
import {Http, HTTP_PROVIDERS} from 'angular2/http';
import 'rxjs/add/operator/map';

@Component({
  selector: 'people',
  //viewProviders: [HTTP_PROVIDERS],
  templateUrl: 'app/templates/people.html',

})

export class PeopleComponent {

  people = [];

  self = this;

  constructor(http: Http) {

    console.log('test',  http.get('people.json'));

    console.log(this);
    //var thiss = this;

    http.get('ajax_responses/people.json')
      // Call map on the response observable to get the parsed people object
      .map(res => res.json())
      // Subscribe to the observable to get the parsed people object and attach it to the
      // component
     /* .subscribe(people => this.people = people, function(){}, function() {

        // on complete
        console.log(self.people);
      });*/

        // this method draws instantly in the template the information
    .subscribe(people => this.people = people);



  }
}