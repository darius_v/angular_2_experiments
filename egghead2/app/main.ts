/**
 * Egghead 2
 */
import {bootstrap} from 'angular2/platform/browser'


import {PeopleComponent} from "./peopleComponent"

//import {RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS} from 'angular2/router';

import {AppComponent}     from './app.component';
import {ROUTER_PROVIDERS} from 'angular2/router';

// this is needed for AJAX in PeopleComponent
import {Http, HTTP_PROVIDERS} from 'angular2/http';

// for Component decorator
import {Component} from 'angular2/core';

/*
@Component({
    selector:'my-app',
    template: `<div>My first Angular 2 Appzz</div>` +
    // todo-input is field and submit button
    '<todo-input></todo-input>' +
    '<todo-list></todo-list>' +

        // router-outlet to the shell template where views will be displayed
        // https://angular.io/docs/ts/latest/guide/router.html
    `<div>
    <router-outlet></router-outlet>
    </div>`,
    directives: [TodoInput, TodoList],
})*/



/*
class AppComponent { }

@RouteConfig([
    {path: '/', name: 'HomePage', component: AppComponent},
    {path: '/ajax', name: 'Ajax', component: PeopleComponent}
])*/




// https://angular.io/docs/js/latest/api/platform/browser/bootstrap-function.html
// when not passing TodoService provider, it throws exception:
// EXCEPTION: No provider for TodoService! (TodoInput -> TodoService)
// directives TodoInput and TodoList user the TodoService. If not using those directices - then code works without TodoService 
// provider
//bootstrap(AppComponent, [TodoService, ROUTER_PROVIDERS]);


bootstrap(AppComponent, [
    ROUTER_PROVIDERS, // for AppComponent
    HTTP_PROVIDERS, // for PeopleComponent

]);



//bootstrap(PeopleComponent, [Http, HTTP_PROVIDERS]);