import {Component} from 'angular2/core';
import {RouteConfig, ROUTER_DIRECTIVES} from 'angular2/router';


import {PeopleComponent} from "./peopleComponent"
import {TodoComponent} from "./todo_service/todoComponent"
import {NameParentComponent} from "./component_communication/nameParentComponent"


@Component({
    selector: 'my-app',
    template: `
    <h1>Component Router</h1>
    <nav>
        <a [routerLink]="['Home']">Home</a>
        <a [routerLink]="['People']">Ajax - people</a>
        <a [routerLink]="['ComponentCommunication']">Component communication</a>
    </nav>
    <router-outlet></router-outlet>
  `,

    directives: [ROUTER_DIRECTIVES]
})



@RouteConfig([
    {path:'/', name: 'Home', component: TodoComponent},
    {path:'/ajax',name: 'People', component: PeopleComponent},
    {path:'/component-comunication',name: 'ComponentCommunication', component: NameParentComponent}
])
export class AppComponent { }