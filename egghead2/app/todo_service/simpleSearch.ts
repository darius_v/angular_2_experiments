import {Pipe, PipeTransform} from "angular2/core";

@Pipe({
    name: 'simpleSearch',
    // to update pipe when smth else updates - in this case when we add new model to array
    // otherwise updates only when field or letter changes (2nd param)
    pure: false
})

// to use in the template - we have to add to @component:
// pipes: [SimpleSearch]
export class SimpleSearch implements PipeTransform {


    // https://angular.io/docs/ts/latest/guide/pipes.html
	/**
     *
     * @param items - array of objects which we reduce
     * @param fields - array of fields of object. Like if object is {title: 'titleval', name: 'nameval'},
     * the fields is [title, name]
     * @param needleString - string we are searching
     * @returns {any} - array which items matches search term
     */
    // in brackets after items is array of arguments which we pass to pipe
    // itemsArray | simpleSearch: ['field1', 'field2']:'searchterm'
    transform(items, [fields, needleString]:[string[], string]) {

        // .filter is javacript native function, which accepts callback
        // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter
        // some() loops trough the fields. Filtering throug all of the values, getting the item of each of them,
        // if the item[field] includes string we are searching, it will return that item
        // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/some
        // includes() is js function https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/includes
        // checks if needleString exists in a string
        return items.filter((item) => fields.some((field) => item[field].includes(needleString)))
        // () => is function syntax. This can be written this way also:
        /*
        return items.filter(function(item) {
            return fields.some((field) => item[field].includes(letter));
        })*/

    }
}
