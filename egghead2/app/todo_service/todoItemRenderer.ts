/**
 * Created by Darius on 2015.10.29.
 */
import {Component, Input} from "angular2/core";

import {NgClass} from "angular2/common";
import {TodoModel} from "./todoService";

@Component({
    selector: 'todo-item-renderer',
	// example has this directive, but it works without it.
	//directives:[NgClass],

	// https://angular.io/docs/ts/latest/api/common/NgClass-directive.html
	// The NgClass directive conditionally adds and removes CSS classes on an HTML element based on an expression's evaluation result.
    template: `

        <div>
        	{{testRenamed}}
        	<!-- bankName - to show example how to pass string from attribute -->
        	{{bankName}}
            {{ todo.number}}
            <span [ngClass]="todo.status">
            {{todo.title | uppercase}}
            </span>
            {{todo.status}}
            <button (click)="todo.toggle()">toggle</button>
        </div>

    `,
	styles: [`
		.completed {
		  text-decoration: line-through;
		}`
	]
})

/* Renders a TodoModel
* https://angular.io/docs/ts/latest/cookbook/component-communication.html
*
* https://angular.io/docs/ts/latest/api/core/Input-var.html
*/
export class TodoItemRenderer {

	// example to pass TodoModel:
	// <todo-item-renderer [todo]="todoService.todos[0]"></todo-item-renderer>
    @Input() todo:TodoModel;

	// can pass data through attribute this way also, but did not work with model. With model needed [].
	// [] is needed always when we want to pass a property from class.
	// when we do not use [], we pass string value, like in this case bank-name
	// <todo-item-renderer bank-name="RBC" [todo]="todoService.todos[0]"></todo-item-renderer>
	@Input('bank-name') bankName: string;

	//  <todo-item-renderer [test]="testProperty"></todo-item-renderer>
	// so in our template we use testRenamed to print it
	@Input('test') testRenamed: string;

}