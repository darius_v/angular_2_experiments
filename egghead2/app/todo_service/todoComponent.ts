/**
 * Created by darius on 07/03/16.
 */


import {TodoList} from "./todoList"
import {TodoInput} from "./todoInput"

import {Component} from 'angular2/core';

// because TodoInput uses it. TodoService holds array of todos, has function to add item
import {TodoService} from './todoService'


console.log('todoComponent');
@Component({

    // for TodoInput. Otherwise:
    // EXCEPTION: No provider for TodoService! (TodoInput -> TodoService)
    providers: [TodoService],
    // works also with this:
    // viewProviders: [TodoService],
    // 'Array of dependency injection providers scoped to this component's view.'
    // from docs: 'Defines the set of injectable objects that are visible to its view DOM children.'
    // https://angular.io/docs/ts/latest/api/core/ComponentMetadata-class.html


    template: `<div>My first Angular 2 Appzz</div>` +
        // todo-input is field and submit button
        '<todo-input></todo-input>' +
        '<todo-list></todo-list>',

    // directives place components instead of tags in <todo-input> and <todo-list>
    // the tags have to match the selector option in @Component without < >
    // directives as I understand are components like this component.
    // And this component is also a directive in app.components.ts - its a router directive.
    directives: [TodoInput, TodoList],
})

// lets user enter new todos and lists them.
// use for home page
export class TodoComponent {

}