

// for Component decorator
import {Component} from 'angular2/core'  

@Component({
    selector: 'letter-select',
	
	templateUrl: 'app/templates/letterSelect.html'

})


export class LetterSelect {
    letters:string[] = ['e', 'c', 's']; 

    // this is connected to [(ngModel)]="selectedLetter"
    selectedLetter:string = 'e';
}