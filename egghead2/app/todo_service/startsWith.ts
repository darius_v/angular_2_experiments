/**
 * Created by Darius on 2015.10.30.
 */
import {Pipe} from "angular2/core";

// checkc SimpleSearch.ts to see comments about pipes
@Pipe({
    name: 'startsWith',
    // to update pipe when smth else updates - in this case when we add new model to array
    // otherwise updates only when field or letter changes (2nd param)
    pure: false
})

export class StartsWith{
    transform(value, [field, letter]) {
        //console.log(value);
        return value.filter((item) => item[field].startsWith(letter));
    }
}