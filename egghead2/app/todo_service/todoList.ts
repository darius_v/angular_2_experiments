/**
 * Created by Darius on 2015.10.26.
 */
//import {Component, View, NgFor} from "angular2/angular2";
import {TodoService} from "./todoService"; 
import {TodoItemRenderer} from "./todoItemRenderer";
import {StartsWith} from "./startsWith";
import {LetterSelect} from "./letterSelect";
import {TodoSearch} from "./todoSearch"; 
import {SimpleSearch} from "./simpleSearch";
// for Component decorator
import {Component} from 'angular2/core'  


@Component({
    selector: 'todo-list',
	directives: [TodoItemRenderer, LetterSelect, TodoSearch],


    pipes: [StartsWith, SimpleSearch],

    // now we can use diferent renderers, for example for
    // style change
    template: '<div>' +
        '<todo-search #todoSearch></todo-search>' +
        '<todo-item-renderer ' +

        // letterSelect.selectedLetter - letterSelect is #letter-select in letter-select component
        // todoSearch.term comes from #todo-search

        `*ngFor="#todo of todoService.todos | simpleSearch: ['title']:todoSearch.term" [todo]="todo">` +
        // why does not refresh when is pipe on adding new item - this would do when pure is set to false in directive.
        //`*ng-for="#todo of todoService.todos | startsWith:'title':'c'" [todo]="todo">` +
        
        // error - probaly because lowercase wants string but gets object
        //`*ng-for="#todo of todoService.todos | lowercase" [todo]="todo">` +
        '</todo-item-renderer>' +

        `<div class="block">
            First todo model rendered with additional string data from attribute:
            <!-- to [test] is assigned property of class, not the string.
             To bank-name is assigned string
             -->
            <todo-item-renderer [test]="testProperty" bank-name="RBC" [todo]="todoService.todos[0]"></todo-item-renderer>
        </div>` +



         // draws selected letter which we select in dropdown. Recognizes by #letterSelect variable - from this we can
         // access all letterSelect class properties
        
            '<letter-select #letterSelect></letter-select>' +
            'Example of displaying value outside of component: <br>' +
            '{{letterSelect.selectedLetter}}<br>' +
            'Filtered by first letter selected and search: <br>' +
            '<div class="block">' +
           '<todo-item-renderer ' +

               `*ngFor="#todo of (todoService.todos | startsWith:'title':letterSelect.selectedLetter) |  simpleSearch: ['title']:todoSearch.term" [todo]="todo">` +
               
           '</todo-item-renderer></div>' +
        '</div>' 
       
       

})
/*
@View({
    directives: [NgFor, TodoItemRenderer, LetterSelect, TodoSearch],

    // after update does not work
    //pipes: [StartsWith, SimpleSearch],

    // now we can use diferent renderers, for example for
    // style change
    template: '<div>' +
        '<todo-search #todo-search></todo-search>' +
        '<todo-item-renderer ' +

        // letterSelect.selectedLetter - letterSelect is #letter-select in letter-select component
        // todoSearch.term comes from #todo-search
        //`*ng-for="#todo of (todoService.todos | startsWith:'title':letterSelect.selectedLetter) | simpleSearch: 'title':todoSearch.term" [todo]="todo">` +
        //`*ng-for="#todo of todoService.todos | simpleSearch: ['title', 'number']:todoSearch.term" [todo]="todo">` +
        `*ng-for="#todo of todoService.todos | simpleSearch: ['title', 'number']:todoSearch.term" [todo]="todo">` +
        // why does not refresh when is pipe on adding new item - this would do when pure is set to false in directive.
        //`*ng-for="#todo of todoService.todos | startsWith:'title':'c'" [todo]="todo">` +
        
        // error - probaly because lowercase wants string but gets object
        //`*ng-for="#todo of todoService.todos | lowercase" [todo]="todo">` +
        '</todo-item-renderer>' +
        '</div>'
        //'<letter-select #letter-select></letter-select>'

})
*/
export class TodoList{

    public testProperty = 'Property in the TodoList class.'

    constructor(
        public todoService:TodoService

    ){}

}