/**
 * Created by Darius on 2015.10.31.
 */

import {FORM_DIRECTIVES} from "angular2/common";
import {Component} from "angular2/core";

@Component({
    selector: 'todo-search',
    // for ng-model
    directives: [FORM_DIRECTIVES],
    template:
        'Search: <input type="text" [(ngModel)]="term">'
})


export class TodoSearch {
    term:string = "";
}
