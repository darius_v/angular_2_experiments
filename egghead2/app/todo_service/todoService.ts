/**
 * Created by Darius on 2015.10.25.
 */

// trying to solve EXCEPTION: No provider for TodoService! (TodoInput -> TodoService)
// import {TodoInput} from "./todoInput"

export class TodoModel{
    status:string = "started";
   
    constructor(
        public title:string = "",
        public number:number = 1
    ){

    }

    toggle():void{
        if(this.status == 'started') this.status = 'completed';
        else this.status = 'started';
        console.log(this.status, this.title);
    }
}

/**
 * Holds array of todos
 */
export class TodoService {

    // after update - error: cannot find name 'integer'
    //number:integer = 1;
    currentNumber:number = 1;

    todos:TodoModel[] = [];

    constructor() {
        console.log("todo service constructor");

        this.addTodo(new TodoModel('code', this.currentNumber));
    }


    addTodo(todo:TodoModel):void {

        this.todos.push(todo);
        this.currentNumber++;
        console.log(this.todos);
    }

   

    /*
    toggleTodo(todo:TodoModel){

    }*/

}
