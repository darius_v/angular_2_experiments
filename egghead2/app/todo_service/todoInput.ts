/**
 * Created by Darius on 2015.10.24.
 */

// TodoService holds array of TodoModels
import {TodoService, TodoModel} from "./todoService";

// for Component decorator
import {Component} from 'angular2/core'

@Component({

    selector: 'todo-input',
    template: `<input type="text" #logMe>` +

    // logMe.value is taken from input having #logMe
    // http://learnangular2.com/events/
    // Events in Angular 2 use the parentheses notation in templates
    `<button (click)="onClick(logMe.value)" (mouseover)="onMouseOver($event, logMe)">Add todo</button>`

})

export class TodoInput{

    constructor(
        // todoService automatically becomes property of TodoInput class
        public todoService:TodoService
    ) {
        console.log(this.todoService);
    }

    onClick(title) {

        console.log('title', title)
        // maybe need getter function for currentNumber
        this.todoService.addTodo(new TodoModel(title, this.todoService.currentNumber));

    }

    // example of getting mouse event
    onMouseOver(event, button) {
        console.log(event, button);
    }
}
